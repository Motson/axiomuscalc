<?php
namespace classes\ApiRequest;

class CalcApiRequest extends AbstractApiRequest
{
    public $data = [
        'type_company' => null,
        'weight' => null,
        'x' => null,
        'y' => null,
        'z' => null,
        'city' => null,
        'area' => null,
        'region' => null,

        'code' => null,
        'tariff_zone' => null,
    ];

    public function getPage() {
        return 'calc.php';
    }
}