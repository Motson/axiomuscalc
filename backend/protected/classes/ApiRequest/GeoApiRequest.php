<?php
namespace classes\ApiRequest;

class GeoApiRequest extends AbstractApiRequest
{
    public $data = [
        'lan'=>null,
        'lat'=>null,
        'limi'=>1,
        'show_companies'=>true,
    ];

    public function getPage() {
        return 'api_geo.php';
    }

}