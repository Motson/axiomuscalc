<?php
namespace classes\ApiRequest;

use classes\LogTrait;

abstract class AbstractApiRequest
{
    use LogTrait;
    const BASE_URL = 'https://axiomus.ru/calc/';
    const TOKEN = '76793d5test0cf77';

    const COMPANY_AKSIOMUS= 0;
    const COMPANY_POST_OF_RUSSIA = 1;
    const COMPANY_BOXBERRY = 51;
    const COMPANY_TOP_DELIVERY = 4;
    const COMPANY_DPD = 2;

    protected $data = null;

//    public $data = [
//        'type_company' => null,
//        'weight' => null,
//        'x' => null,
//        'y' => null,
//        'z' => null,
//        'city' => null,
//        'area' => null,
//        'region' => null,
//
//        'code' => null,
//        'tariff_zone' => null,
//    ];


//    public function setData($data) {
//        $this->data = $data;
//    }

//    public function getData() {
//        $this->_data;
//    }

    abstract function getPage();

    public function getUrl() {
        return self::BASE_URL . $this->getPage();
    }

    public function query($method) {
        header("Content-type: application/json; charset=utf-8");
        if(empty($this->data)) {
            throw new \Exception('Empty request data');
        }
        $data = array_merge(['token' => self::TOKEN, 'method' => $method], $this->data);
        $options = ['http' =>	array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($data))];

        $context = stream_context_create($options);

        if($response = file_get_contents($this->getUrl(), false, $context)){
            $result = json_decode($response,true);
            $this->queryLog(http_build_query($data));
        } else {
            $result = ['result'=>false, 'message'=>'Ошибка отправки запроса!'];
        }

        return $result;
    }



    public function fillByGeocoder() {
        $address = $this->data['address'];

        $yaGeo = self::yandexGeocoder($address);
        if(empty($yaGeo)) {
            throw new \Exception('Empty yandexGeo');
        }

        $this->data['city'] = $yaGeo['city'];
        $this->data['area'] = $yaGeo['area'];
        $this->data['region'] = $yaGeo['region'];
        $this->data['lon'] = $yaGeo['lon'];
        $this->data['lat'] = $yaGeo['lat'];
    }

    protected static function yandexGeocoder($address){

        $url = 'https://geocode-maps.yandex.ru/1.x/?geocode='.urlencode(trim($address)).'&amp;format=json&amp;results=1';
        //+ '&key=' + urlencode('API-ключ если у Вас он есть, без него разрешено делать 25000 запросов в день');

        // Получаем ответ от Яндекс.Геокодера и преобразуем в ассоциативный массив
        $response = file_get_contents($url);
        $resp = json_decode($response, true);
        $ya_data = $resp['response']['GeoObjectCollection'];

        // Если удалось определить координаты, составляем выходной массив $g, иначе возвращаем false (Ошибка геокодирования)
        if($ya_data['metaDataProperty']['GeocoderResponseMetaData']['results'] == 1){
            if(isset($ya_data['featureMember'][0]['GeoObject']['Point']['pos']) AND trim($ya_data['featureMember'][0]['GeoObject']['Point']['pos'])!=''){
                $tmp = explode(' ', $ya_data['featureMember'][0]['GeoObject']['Point']['pos']);
                $g['lon'] = $tmp[0]; // Долгота
                $g['lat'] = $tmp[1]; // Широта
                if(isset($ya_data['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['AdministrativeAreaName'])){
                    $g['area'] = $ya_data['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['AdministrativeAreaName']; // Область
                }else{
                    $g['area'] = '';
                }
                if(isset($ya_data['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['SubAdministrativeAreaName'])){
                    $g['region'] = $ya_data['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['SubAdministrativeAreaName']; // Регион
                }else{
                    $g['region'] = '';
                }
                if(isset($g['region']) AND $g['region']!=='' AND isset($ya_data['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName'])){
                    $g['city'] = $ya_data['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['SubAdministrativeArea']['Locality']['LocalityName']; // Город
                }else{
                    if(isset($ya_data['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['LocalityName'])){
                        $g['city'] = $ya_data['featureMember'][0]['GeoObject']['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['Locality']['LocalityName']; // Город
                    }
                }
                if(!isset($g['city']) OR $g['city'] === ''){
                    $g['city'] = NULL; // Если город неопределен возвращаем city = NULL
                }
                unset($tmp);
                return $g;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

}