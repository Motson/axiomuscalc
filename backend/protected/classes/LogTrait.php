<?php
namespace classes;

trait LogTrait
{
    public function queryLog($queryString) {

        $manager = new \MongoDB\Driver\Manager(Params::get('connection'));
        $dbName = Params::get('database');

        $bulk = new \MongoDB\Driver\BulkWrite;
        $bulk->update(
            ['query'=>$queryString],
            ['$inc'=>['count' => 1]]
        );

        $result = $manager->executeBulkWrite("$dbName.queries", $bulk);
        if($result->getModifiedCount() == 0) {
            $bulk = new \MongoDB\Driver\BulkWrite;
            $bulk->insert(['query'=>$queryString, 'count'=>1]);
            $manager->executeBulkWrite("$dbName.queries", $bulk);
        };
    }
}