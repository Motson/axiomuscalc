<?php
namespace classes;

class Params
{
    private static $_data = null;

    public static function get($param) {

        if (self::$_data == null) {
            self::$_data = require_once(__DIR__.'/../config.php');;
        }

        if(!isset(self::$_data[$param])) {
            throw new \Exception('Config param not found');
        }
        return self::$_data[$param];
    }
}