<?php
use classes\ApiRequest\{CalcApiRequest, GeoApiRequest};
require_once "../../protected/autoload.php";

$result = ['success'=>false, 'errorMessage'=>'', 'companies'=>[]];
$data = $_POST;

try {

    if(empty($data)) {
        throw new Exception('Empty input data');
    }
    $data['city'] = $_POST['city']=='moscow' ? "Москва" : 'Санкт-Петербург';

    $companies = [
        CalcApiRequest::COMPANY_AKSIOMUS,
        CalcApiRequest::COMPANY_DPD,
        CalcApiRequest::COMPANY_POST_OF_RUSSIA,
        CalcApiRequest::COMPANY_BOXBERRY,
    ];

    $totalResponse = [];
    $calcRequest = new CalcApiRequest();
    $calcRequest->data = $data;
    $calcRequest->fillByGeocoder();

    $geoRequest = new GeoApiRequest();
    $geoRequest->data['lon'] = $calcRequest->data['lon'];
    $geoRequest->data['lat'] = $calcRequest->data['lat'];
    $geoResult = $geoRequest->query('point');

    foreach ($companies as $company) {
        $calcRequest->data['type_company'] = $company;
        $deliveryResponse = $calcRequest->query('delivery');

        // Ищем код и тарифную зону ПВЗ
        foreach ($geoResult['pvz'] as $geoResultItem) {
            if($geoResultItem['type_company'] == $company) {
                $calcRequest->data['code'] = $geoResultItem['code'];
                $calcRequest->data['tariff_zone'] = $geoResultItem['tariff_zone'];
                break;
            }
        }

        if(!empty($calcRequest->data['code'])) {
            $carryResponse = $calcRequest->query('carry');
            $totalResponse = array_merge_recursive($totalResponse, $carryResponse, $deliveryResponse);
        } else {
            $totalResponse = array_merge_recursive($totalResponse, $deliveryResponse);
        }
    }



    // Формируем массив со структурой Компания -> тарифы
    foreach ($totalResponse as $totalResponseKey => $totalResponseItem) {
        $companyItem = [
            'companyName' => $totalResponseKey,
            'tariffs' => []
        ];


        $types = ['delivery', 'carry'];
        foreach ($types as $type) {
            if(!isset($totalResponseItem[$type])) {
                continue;
            }
            foreach ($totalResponseItem[$type] as $resultItem) {
                if(!empty($resultItem['error'])) {
                    continue;
                }

                $tariffKey = array_search($resultItem['tariffName'], $companyItem['tariffs']);
                if(empty($tariffKey)) {
                    $tariffKey = count($companyItem['tariffs']);
                    $companyItem['tariffs'][$tariffKey] = ['name'=>$resultItem['tarifName']];
                }

                $companyItem['tariffs'][$tariffKey][$type] = ['price'=>$resultItem['price'], 'days'=>$resultItem['time']];
            }
        }

        $result['companies'][] = $companyItem;
    }
    $result['success'] = !empty($result['companies']);

    echo json_encode($result);
} catch (Exception $e) {
    $result['errorMessage'] = $e->getMessage();
    echo json_encode($result);
}
