var apiHost = 'api.axiomus.loc';

chrome.runtime.onMessageExternal.addListener(
    function(request, sender, sendResponse) {
        $.ajax({
            method: "POST",
            url: "http://"+apiHost+"/v1/calc.php",
            data: request
        }).done(function(msg) {
            sendResponse(msg);
        });

        return true;
});