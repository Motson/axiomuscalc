$( document ).ready(function() {

    $('#calc-address').kladr({
        oneString: true
    });
    var formElement = $('#axiomus-form');
    var calcDangerElement = $('#calc-danger');

    formElement.submit(function( event, FormObject ) {
        $('.js-calc-ajax-loader').show();
        var button = formElement.children('input[type=submit]');
        button.attr('disabled', 'disabled');
        var inputs = {};
        $.each(formElement.serializeArray(), function(_, kv) {
            inputs[kv.name] = kv.value;
        });

        var hasExtension = false;

        // Отправляем запрос в background
        chrome.runtime.sendMessage('ocgolgdpieoffclebiaoockcodpooecg', inputs, function(response) {
            var html = '';

            if(response) {
                hasExtension = true;
                if(response.success) {
                    response.companies.forEach(function(companyItem, companyIndex, companyArray) {
                        var companyName = companyItem.companyName;
                        companyItem.tariffs.forEach(function (tariffItem, tariffKey, tariffArray) {
                            var deliveryPrice = tariffItem.delivery !== undefined ? 'Цена: '+tariffItem.delivery.price+' <span class="price-rub">₽</span>' : 'Услуга не доступна';
                            var carryPrice = tariffItem.carry !== undefined ? 'Цена: '+tariffItem.carry.price+' <span class="price-rub">₽</span>' : 'Услуга не доступна';
                            var deliveryDays = tariffItem.delivery !== undefined ? 'Дней: '+tariffItem.delivery.days : '';
                            var carryDays = tariffItem.carry !== undefined ? 'Дней: '+tariffItem.carry.days : '';

                            $('.js-calc-ajax-loader').hide();

                            html +=
                                '<div class="col-lg-3 col-md-6 col-12">' +
                                '<div class="calc-result-block text-center">'+
                                '<div class="calc-result-header">' +
                                '<h4>'+companyName+'</h4>'+
                                '<h5>'+tariffItem.name+'</h5>'+
                                '</div>'+

                                '<div class="calc-result-info">' +
                                '<div class="calc-result-title">Доставка курьером</div>'+
                                '<div class="calc-result-price">'+deliveryPrice+'</div>'+
                                '<div class="calc-result-days">'+deliveryDays+'</div>'+

                                '<div class="calc-result-title">Доставка до ПВЗ</div>'+
                                '<div class="calc-result-price">'+carryPrice +'</div>'+
                                '<div class="calc-result-days">'+carryDays+'</div>'+
                                '</div>'+
                                '</div>'+
                                '</div>';
                        });
                    });

                    $('#axiomus-result').html(html);
                }
            } else {
                hasExtension = false;
            }
        });

        if(hasExtension) {
            calcDangerElement.slideUp(500);
        } else {
            calcDangerElement.hide()
        }

        event.preventDefault();
    });

});