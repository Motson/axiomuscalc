Axiomus Calculator
============================

Калькулятор Axiomus: веб-страницы, расширение Google Chrome, бекенд
Стек технологий: PHP7, MongoDB, JS

СТРУКТУРА ДИРЕКТОРИЙ
-------------------

      backend/            contains assets definition
	  -- protected		  защищенная папка с классами и конфинурациями
	  -- www			  точка входа для бекенда
      chrome-extension/   раширение для Google Chrome
      frontend/           веб-страница


КОНФИГУРАЦИЯ
-------------

### База данных

При необходимости отредактировать файл конфигураии MongoDB `backend/config.php` в соответствии с Вашей конфигурацией:
```php
return [
    'database' => 'axiomus',
    'connection' => 'mongodb://localhost:27017',
];
```

### Настройка доменов
По умолчанию расширение обращается к бекенду по адресу api.axiomus.loc. Его можно изменить в `chrome-extension/background.js`:
```js
var apiHost = 'api.axiomus.loc';
```
Так же может понадобится изменить домен веб-страницы в `manifest.json`:

```js
  "externally_connectable": {
    "matches": ["*://localhost/*", "*://axiomus.loc/*"]
  },
```
